import React, {useRef} from 'react';
import { NavLink } from "react-router-dom";

export default function Navbar({ props }) {

    return (
        <header className="py-3 border-b-2 border-gray-100 bg-white 500">
            <div className="container mx-auto flex flex-wrap items-center justify-between">
                <p className="font-bold">Dreamhouselab</p>
            </div>
        </header>
    );
}

export { Navbar };