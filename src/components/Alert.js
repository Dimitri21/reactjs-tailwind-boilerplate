import React from 'react';

function Alert(props) {
    return (
        <div class="text-white px-6 py-2 border-0 rounded relative bg-red-500">
            <span class="text-xl inline-block mr-5 align-middle">
                <i class="fas fa-exclamation-circle" />   
            </span>
            <span class="inline-block align-middle mr-8 capitalize">
                {props.text}
            </span>
        </div>
    );
}

export { Alert };