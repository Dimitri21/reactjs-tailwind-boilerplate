import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';

import { history } from './helpers';
import { PrivateRoute } from './components';
import { Navbar } from './layouts'

import  Login from './pages/Login/index';
import  Home from './pages/Login/index';
import  NotFound from './pages/NotFound/index';

function App() {
    
    return (
        <Router history={history}>
            <div className="flex flex-col h-screen">
            <Navbar />
            <div className="flex-1 overflow-y-auto">
                <div className={'app'}>
                    <Switch>
                        <PrivateRoute path="/" component={Home} />
                        <Route exact path="/login" component={Login} />
                             
                        <Route component={NotFound} />
                    </Switch>   
                </div>
            </div>
            </div>
        </Router>
    );
}

export { App };