import React from 'react';

function Home(props) {
    return (
        <div className="flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
            <div className="max-w-md w-full space-y-8">
                <div>
                    <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
                        Welcome!
                    </h2>
                </div>
                <div className="mt-8 space-y-6">
                    <p className={'text-center text-gray-500 my-3'}>ReactJS with TailwindCSS Boilerplate by Dreamhouselab</p>
                    <div>
                        <button className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Logout
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;