import React, {useState, useEffect} from 'react';

import { useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { userActions } from '../../actions';

import {Alert} from '../../components'

function Login(props) {

    const [inputs, setInputs] = useState({
        email: '',
        password: ''
    });
    const { email, password } = inputs;

    const loggingIn = useSelector(state => state.authentication.loggingIn);
    const failure = useSelector(state => state.authentication.failure);
    
    const dispatch = useDispatch();
    const location = useLocation();

    useEffect(() => { 
        dispatch(userActions.logout()); 
    }, []);

    function handleChange(e) {
        const { name, value } = e.target;
        setInputs(inputs => ({ ...inputs, [name]: value }));
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        
        if (email && password) {
            // get return url from location state or default to home page
            const { from } = location.state || { from: { pathname: "/" } };
            dispatch(userActions.login(email, password, from));
        }
    }

    console.log(failure)

    return (
        <div className="flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
            <div className="max-w-md w-full space-y-8">
                <div>
                    <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
                        Sign in to your account
                    </h2>
                    <p className={'text-center text-gray-500 my-3'}>ReactJS with TailwindCSS Boilerplate by Dreamhouselab</p>
                </div>
                <form className="mt-8 space-y-6" onSubmit={handleSubmit}>
                    <input type="hidden" name="remember" value="true" />
                    <div className="rounded-md shadow-sm -space-y-px">
                        <div>
                            <label htmlFor="email-address" className="sr-only">Email address</label>
                            <input 
                                onChange={handleChange} 
                                required 
                                id="email-address" 
                                name="email" 
                                type="email" 
                                autoComplete="email" 
                                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm" 
                                placeholder="Email address" 
                            />
                        </div>
                        <div>
                            <label htmlFor="password" className="sr-only">Password</label>
                            <input 
                                onChange={handleChange}  
                                required 
                                id="password" 
                                name="password" 
                                type="password" 
                                autoComplete="current-password" 
                                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm" 
                                placeholder="Password" 
                            />
                        </div>  
                    </div>
                    <div>
                        <button disabled={loggingIn} type="submit" className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            {loggingIn ? 'Logging in...' : 'Sign in'}
                        </button>
                    </div>
                    {failure && <Alert text={failure}/>}
                </form>
            </div>
        </div>
    );
}

export default Login;